<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\gallery_sub_categiry;
use App\Models\gallery_category;

class GallerysubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gsc=gallery_sub_categiry::with('gallleryCategory')->get();
        return view('Admin.gallarysub.index',compact('gsc'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gc = gallery_category::all();
        return view('Admin.gallarysub.gallarysub',compact('gc'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gsc = new gallery_sub_categiry();
        $gsc->name = $request->name;
        $gsc->gc_id = $request->gc_id;
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $gsc->image_name = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        $gsc->save();
        return redirect()->route('gallarysub.index');
        return back()->with('message','Data save successfully ');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gc = gallery_category::all();
        $gsc = gallery_sub_categiry::find($id);
        return view('Admin.gallarysub.gallarysub',compact('gsc','gc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gsc = gallery_sub_categiry::find($id);
        $gsc->name = $request->name;
        $gsc->gc_id = $request->gc_id;
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $gsc->image_name = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        $gsc->save();
        return redirect()->route('gallarysub.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gsc = gallery_sub_categiry::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
}
