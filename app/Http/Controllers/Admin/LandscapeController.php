<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Landscape;
use App\Models\Company;

class LandscapeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $data['cc']= Landscape::with('company')->get();
         return view('Admin.Landscape.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $company = Company::all();
       return view('Admin.Landscape.add_Landscape',compact('company')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $townp = new Landscape();
        $townp->company_id = $request->company_id;
        $townp->plot_location = $request->plot_location;
        $townp->covered_area = $request->covered_area;
        $townp->rate_sqft = $request->rate_sqft;
        $townp->total = $request->total;
        $townp->details = $request->details;
        $townp->get_quotation = $request->get_quotation;
        $townp->professional_location = $request->professional_location;
        $townp->plot_type = $request->plot_type;
        
        
        $townp->save();
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::all();
          $arch = Landscape::find($id);
        return view('Admin.Landscape.add_Landscape',compact('arch','company'));
        dd($arch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $townp = Landscape::find($id);
        $townp->company_id = $request->company_id;
        $townp->plot_location = $request->plot_location;
        $townp->covered_area = $request->covered_area;
        $townp->rate_sqft = $request->rate_sqft;
        $townp->details = $request->details;
        $townp->total = $request->total;
        $townp->get_quotation = $request->get_quotation;
        $townp->professional_location = $request->professional_location;
        $townp->plot_type = $request->plot_type;
        
        $townp->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $arch = Landscape::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
}
