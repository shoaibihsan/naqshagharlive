<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::paginate(20);
        return view('Admin.company.index', compact('company'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        $company->c_name = $request->c_name;
        $company->c_type = $request->c_type;
        $company->c_drawing_detail = $request->c_drawing_detail;

        if ($request->file('c_profile_image')) {
            $file = $request->c_profile_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $company->c_profile_image = $filename;
            $path = public_path('images/company_profile');
            $file->move($path, $filename);
        }
        $company->save();

        return redirect()->route('company.index');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('Admin.company.create', compact('company'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->c_name = $request->c_name;
        $company->c_type = $request->c_type;
        $company->c_drawing_detail = $request->c_drawing_detail;

        if ($request->file('c_profile_image')) {
            $file = $request->c_profile_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $company->c_profile_image = $filename;
            $path = public_path('images/company_profile');
            $file->move($path, $filename);
        }

        $company->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id)->delete();
        return back()->with('message', 'Record deleted successfully ');
    }
}
