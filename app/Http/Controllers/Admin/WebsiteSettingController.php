<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\settings;

class WebsiteSettingController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = settings::paginate(20);   
        return view('Admin.WebsiteSetting.index',compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.WebsiteSetting.add_logo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $settings = new settings();

        $settings->title = $request->title;
        
        if ($request->file('logo')) {
            $file = $request->logo;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->logo = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        if ($request->file('banner_image')) {
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->banner_image = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }

        
         $settings->save();
         return redirect()->route('setting.index');
         return back()->with('message','Data save successfully ');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = settings::find($id); 
        return view('Admin.WebsiteSetting.add_logo',compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $settings = settings::find($id);

        $settings->title = $request->title;
        
        if ($request->file('logo')) {
            $file = $request->logo;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->logo = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        if ($request->file('banner_image')) {
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->banner_image = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        
         $settings->save();
         return redirect()->route('setting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = settings::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
}
