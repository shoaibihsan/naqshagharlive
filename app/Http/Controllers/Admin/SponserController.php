<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\sponsored_ads;
class SponserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gc=sponsored_ads::all();
        return view('Admin.sponseradd.index', compact('gc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gc=sponsored_ads::all();
      
        return view('Admin.sponseradd.sponseradd',compact('gc'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $settings = new sponsored_ads();

     
        $settings->name = $request->name;
        // $settings->image_name = $request->image_name;
        
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->image_name = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        

        // dd($settings);
         $settings->save();
         // return redirect()->route('company-profile.index');
         return back()->with('message','Data save successfully ');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         {
        $company = sponsored_ads::all();
        $arch = sponsored_ads::find($id);
        return view('Admin.sponseradd.sponseradd',compact('arch','company'));
        // dd($arch);
    }
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          {
        // dd($request->all);
        $townp = new sponsored_ads();
       
        $townp->name = $request->name;
        $townp->image_name = $request->image_name;
        $townp->save();
        return back();  
    }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         {
        $arch = sponsored_ads::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
        //
    }
}
