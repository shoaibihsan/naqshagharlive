<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class gallery_sub_categiry extends Model
{
	protected $table="gallery_sub_categiries";
	protected $primarykey ="id";

	public function gallleryCategory(){
		return $this->belongsTo(gallery_category::class,'gc_id');
	}
    
}
