@extends('website.master-layout')

@push('css')

@endpush

@section('content')

<div class="overview-bgi listing-banner faq-header-div">
   <div class="container listing-banner-info">
      <div class="row">
         <div class="col-lg-12 col-md-12 clearfix">
            <div class="text">
               <!--<h1> FAQ </h1>-->
            </div>
         </div>
      </div>
   </div>
</div>
<div class="listing-details-page content-area-6 pt-0">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <!-- listing description start -->
            <div class="listing-description mb-40" id="about_us_description">
               <p></p>
               <article id="post-5900" class="post-5900 page type-page status-publish has-post-thumbnail hentry">
                  <header class="entry-header">
                  </header>
                  <!-- .entry-header -->
                  <div class="entry-content">
                     <p>
                        <!-- listing description start -->
                     </p>
                     <div id="about_us_description" class="listing-description">
                        <article id="post-4548" class="post-4548 page type-page status-publish has-post-thumbnail hentry">
                           <header class="entry-header"></header>
                           <p>
                              <!-- .entry-header -->
                           </p>
                           <div class="entry-content">
                              <div class="faq content-area-9">
                                 <div class="container">
                                    <p>
                                       <!-- Main title -->
                                    </p>
                                    <div class="main-title text-center">
                                       <h1>Frequently Asked Questions</h1>
                                    </div>
                                    <div class="row">
                                       <div class="col-lg-5 mr-2 ml-2">
                                          <h3 class="mb-3 text-center">For Public</h3>
                                          <div id="faq" class="faq-accordion">
                                             <div class="card m-b-0">
                                                <div class="card-header">
                                                   <a class="card-title collapsed" href="#collapse1" data-toggle="collapse" data-parent="#faq">
                                                      What services do NAQSHA GHAR provides?
                                                   </a>
                                                </div>
                                                <div id="collapse1" class="card-block collapse">
                                                   <div class="p-text">
                                                      Naqsha.com.pk provides connecting services for building professionals and clients, building professional’s rates and contact details are displayed on our website so that people can easily get rates according to their budget in just one click.
                                                   </div>
                                                </div>
                                                <div class="card-header">
                                                   <a class="card-title collapsed" href="#collapse2" data-toggle="collapse" data-parent="#faq">
                                                      Can I select professionals from different area or city?
                                                   </a>
                                                </div>
                                                <div id="collapse2" class="card-block collapse">
                                                   <div class="p-text">
                                                      Yes, you can select any professional from 10 cities of Pakistan from different areas.
                                                   </div>
                                                </div>
                                                <div class="card-header">
                                                   <a class="card-title collapsed" href="#collapse3" data-toggle="collapse" data-parent="#faq"><br>
                                                      How can I find Architects, Builders, Interiors, landscape &amp; town planners?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse3" class="card-block collapse">
                                                   <div class="p-text">
                                                      Select your desire service, fill required information and press search, a list of professionals will appear.
                                                   </div>
                                                </div>
                                                <div class="card-header bd-none">
                                                   <a class="card-title collapsed" href="#collapse4" data-toggle="collapse" data-parent="#faq"><br>
                                                      Can I find professionals for my commercial project?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse4" class="card-block collapse">
                                                   <div class="p-text">Yes, Naqsha Ghar is providing connecting services for both residential and commercial projects.</div>
                                                </div>
                                                <div class="card-header"><a class="card-title collapsed" href="#collapse5" data-toggle="collapse" data-parent="#faq"><br>
                                                   Can we see projects of listed professionals?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse5" class="card-block collapse">
                                                   <div class="p-text">Yes, you can view previous projects of professionals by viewing their portfolios.</div>
                                                </div>
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse6" data-toggle="collapse" data-parent="#faq"><br>
                                                   Are these rates already established or varies according to each professional?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse6" class="card-block collapse">
                                                   <div class="p-text pb-0">Mentioned rates are provided by professionals separately.</div>
                                                </div>
                                                <div class="card-header"><a class="card-title collapsed" href="#collapse7" data-toggle="collapse" data-parent="#faq"><br>
                                                   Can we generate multiple quotations?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse7" class="card-block collapse">
                                                   <div class="p-text">Yes, you can generate multiple quotations to see different rates of different professionals.</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-6 mr-2 ml-2">
                                          <h3 class="mb-3 text-center">For Professional</h3>
                                          <div id="faq" class="faq-accordion">
                                             <div class="card m-b-0">
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse13" data-toggle="collapse" data-parent="#faq"><br>
                                                   How can I register on NAQSHA GHAR?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse13" class="card-block collapse">
                                                   <div class="p-text">After filling the registration form and paying Rs/ 5800 for other cities and Rs/ 10000 for Lahore including taxes   your portfolio will be displayed.</div>
                                                </div>
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse14" data-toggle="collapse" data-parent="#faq"><br>
                                                   For how long I will be registered?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse14" class="card-block collapse">
                                                   <div class="p-text">You will be registered for the lifetime on our website.</div>
                                                </div>
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse15" data-toggle="collapse" data-parent="#faq"><br>
                                                   How do we make sure our registration process is completed?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse15" class="card-block collapse">
                                                   <div class="p-text">Once your registration fees will be cleared/ received, your profile will be uploaded within 3 working days and you can make sure by viewing it on our website.</div>
                                                </div>
                                                <div class="card-header"><a class="card-title collapsed" href="#collapse9" data-toggle="collapse" data-parent="#faq"><br>
                                                   How can I subscribe on NAQSHA GHAR?</a>
                                                </div>
                                                <div id="collapse9" class="card-block collapse">
                                                   <div class="p-text">You can easily subscribe by selecting one of our subscription packages (Silver, Gold, Platinum) for each service.</div>
                                                </div>
                                                <div class="card-header"><a class="card-title collapsed" href="#collapse10" data-toggle="collapse" data-parent="#faq"><br>
                                                   How can we display our rate on NAQSHAGHAR.com?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse10" class="card-block collapse">
                                                   <div class="p-text">To display your rates and contact details on our website subscribe to one of our packages.</div>
                                                </div>
                                                <div class="card-header"><a class="card-title collapsed" href="#collapse11" data-toggle="collapse" data-parent="#faq"><br>
                                                   What payment methods are available?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse11" class="card-block collapse">
                                                   <div class="p-text">You can make your online payments and also can provide cross cheque only.</div>
                                                </div>
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse12" data-toggle="collapse" data-parent="#faq"><br>
                                                   What happens if any quotations will be generated during public holidays or strike?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse12" class="card-block collapse">
                                                   <div class="p-text">Our team will schedule your meeting on working days only.</div>
                                                </div>
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse21" data-toggle="collapse" data-parent="#faq"><br>
                                                   How can I post sponsored advertisement on your website?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse21" class="card-block collapse">
                                                   <div class="p-text">You can have weekly package for <small>(10,000 + 16% GST)</small>, or monthly package for <small>(30,000 + 16% GST)</small> for sponsored advertisement.</div>
                                                </div>
                                                <div class="card-header bd-none"><a class="card-title collapsed" href="#collapse22" data-toggle="collapse" data-parent="#faq"><br>
                                                   Is Naqsha Ghar only registering building professionals who have their own company?<br>
                                                   </a>
                                                </div>
                                                <div id="collapse22" class="card-block collapse">
                                                   <div class="p-text">We are registering companies in only 5 categories (Architecture, Construction, Interior, Landscape and Town Planner). Individual engineers, freelancers and fresh graduates can also get registered in 6th category (Engineers).</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <p>
                              <!-- .entry-content -->
                           </p>
                        </article>
                        <p>
                           <!-- #post-## --><br>
                           <img class="alignnone size-full wp-image-5338" src="https://www.naqsha.com.pk/wp-content/uploads/2019/10/web-banner2-1.png" alt="" width="100%">
                        </p>
                     </div>
                  </div>
                  <!-- .entry-content -->
               </article>
               <!-- #post-## -->
               <p></p>
            </div>
           
            <div class="col-lg-4 col-md-4">
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@push('script')

@endpush