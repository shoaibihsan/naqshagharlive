@extends('website.master-layout')
@push('css')
<style type="text/css">
   .banner h1{
   font-size: 33px;
   }
   h2{
   text-align:center;
   padding: 20px;
   }
   /* Slider */
   .slick-slide {
   margin: 0px 20px;
   }
   .slick-slide img {
   width: 100%;
   }
   .slick-slider
   {
   position: relative;
   display: block;
   box-sizing: border-box;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   -webkit-touch-callout: none;
   -khtml-user-select: none;
   -ms-touch-action: pan-y;
   touch-action: pan-y;
   -webkit-tap-highlight-color: transparent;
   }
   .slick-list
   {
   position: relative;
   display: block;
   overflow: hidden;
   margin: 0;
   padding: 0;
   }
   .slick-list:focus
   {
   outline: none;
   }
   .slick-list.dragging
   {
   cursor: pointer;
   cursor: hand;
   }
   .slick-slider .slick-track,
   .slick-slider .slick-list
   {
   -webkit-transform: translate3d(0, 0, 0);
   -moz-transform: translate3d(0, 0, 0);
   -ms-transform: translate3d(0, 0, 0);
   -o-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   }
   .slick-track
   {
   position: relative;
   top: 0;
   left: 0;
   display: block;
   }
   .slick-track:before,
   .slick-track:after
   {
   display: table;
   content: '';
   }
   .slick-track:after
   {
   clear: both;
   }
   .slick-loading .slick-track
   {
   visibility: hidden;
   }
   .slick-slide
   {
   display: none;
   float: left;
   height: 100%;
   min-height: 1px;
   }
   [dir='rtl'] .slick-slide
   {
   float: right;
   }
   .slick-slide img
   {
   display: block;
   }
   .slick-slide.slick-loading img
   {
   display: none;
   }
   .slick-slide.dragging img
   {
   pointer-events: none;
   }
   .slick-initialized .slick-slide
   {
   display: block;
   }
   .slick-loading .slick-slide
   {
   visibility: hidden;
   }
   .slick-vertical .slick-slide
   {
   display: block;
   height: auto;
   border: 1px solid transparent;
   }
   .slick-arrow.slick-hidden {
   display: none;
   }
   .carousel-item{min-height:300px;  }
   .customer-logos.slider img {
       width:150px;
       height:150px;
   }
   .img_head_mbl img {
    width: 657px;
}
.col-xl-6.col-lg-6.col-md-6.col-sm-12.heading_mob {
    padding: 70px;
}
.container-fluid.new_video_home {
    background: #ededed;
}
.row.simple_search {
    margin-top: 50px;
}
@media(min-width: 375px){
  .slick-slider .slick-track,
   .slick-slider .slick-list
   {
   -webkit-transform: translate3d(0, 0, 0);
   -moz-transform: translate3d(0, 0, 0);
   -ms-transform: translate3d(0, 0, 0);
   -o-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   width:937px;
   margin-left:-140px;
   }
}
@media(min-width: 1024px){
  .slick-slider .slick-track,
   .slick-slider .slick-list
   {
   -webkit-transform: translate3d(0, 0, 0);
   -moz-transform: translate3d(0, 0, 0);
   -ms-transform: translate3d(0, 0, 0);
   -o-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   width:1093px;
   margin-left:-140px;
   }
}

</style>
@endpush
@section('content')
@include('website.includes.home-header-banner')
<!-- Ads Start -->
<div class="listing-item content-area bg-grea-3" style="padding-top: 0px;">
   <div class="container">
      <!-- Main title -->
      <div class="main-title" style="margin-bottom: 0px !important;">
       <!--   <h1 class="pt-4 pb-4" style="margin-bottom: 0px;">Sponsored Ads</h1> -->
      </div>
      <!-- slider -->
      <div class="container-fluid">
           <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
               Sponsored Ads
            </h2>
         </div>
         </div>
         <section class="customer-logos slider">
          
          @if(isset($data['sp']))
          @foreach($data['sp'] as $key => $value )
            <div class="slide">
              <img width="150px" height="150px" src="{{ asset('images/settings'.'/'.$value->image_name) }}">
              <!-- <img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"> -->
            </div>
          @endforeach
          @endif
           <!--  <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div>
            <div class="slide"><img src="{{ asset('public/assets/images/Sponor ads.jpg') }}"></div> -->
         </section>
      </div>
      <!-- Slick slider area start -->
      
   </div>
</div>
<!-- How to get quotation with video-->
<div class="new_video_home">
    <div class="container-fluid new_video_home">
        <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
               Get Professional Constructional Services on Naqsha Ghar
            </h2>
         </div>
         </div>
         <div class="get_quotation">
        <div class="row simple_search">
            <div class="col-xl-6 col-lg-6 col-md-06 col-sm-12">
                <div class="img_head_mbl">
                <img src="public/assets/images/Homepage-Mobile.png"></div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 heading_mob">
                <div class="ul_class">
                <ul>
                    <li>
                        <h5><i class="fa fa-check-circle-o" style="color:#f1ad09;" aria-hidden="true"></i> Simple Search</h5>
                        <p>Search using appropriate filters.Search using appropriate filters.</p>
                    </li>
                    <li>
                        <h5><i class="fa fa-check-circle-o" style="color:#f1ad09;" aria-hidden="true"></i> Check Portfolios</h5>
                        <p>View companies portfolios.</p>
                    </li>
                    <li>
                        <h5><i class="fa fa-check-circle-o" style="color:#f1ad09;" aria-hidden="true"></i> Check Pricing with Location</h5>
                        <p>Select Company</p>
                    </li>
                    <li>
                        <h5><i class="fa fa-check-circle-o" style="color:#f1ad09;" aria-hidden="true"></i> Get Quotation/ Contact Professional</h5>
                        <p>Select your best quotation from result.</p>
                    </li>
                </ul>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- Partner -->
<div class="main-title partners mb-0">
   <div class="container pt-4 pb-4">
      <div class="" style="">
         <h2 class="pb-4 premimum-company">Premium Companies</h2>
      </div>
      <div class="slick-slider-area">
         <div class="row slick-carousel">
            <div class=slick-slide-item>
               <div id="bh" ></div>
            </div>
            <div class=slick-slide-item>
               <div id="bh" ></div>
            </div>
            <div class=slick-slide-item>
               <div id="kb" ></div>
            </div>
            <div class=slick-slide-item>
               <div id="gd" ></div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- Partner -->


<!-- Ads end -->
<!-- Get Quotation Steps start-->
<!--<div id=how-it-works>
   <div class="main-title services content-area">
      <div class="container">-->
         <!-- Main title -->
         <!--<div class="text-center pt-4 pb-4">
            <h1>
               How can you get professionals from our website
            </h1>
            <p>
               Get a quotation in three simple steps.
            </p>
         </div>
         <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 " >
               <div class="service-info" data-aos="fade-right" data-aos-duration="4000">
                  <div>
                     <img src="">  
                  </div>
                  <h3>
                     Simple Search
                  </h3>
                  <p>
                     Search using appropriate filters.
                  </p>
               </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
               <div class="service-info" data-aos="fade-up" data-aos-duration="4000">
                  <div>
                     <img src="">  
                  </div>
                  <h3>
                     Check Portfolios and Pricing
                  </h3>
                  <p>
                     View company portfolios and select your builder/designer.
                  </p>
               </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 " >
               <div class="service-info" data-aos="fade-left" data-aos-duration="4000">
                  <div>
                     <img src="">  
                  </div>
                  <h3>
                     Get Quotation/ Contact Professional
                  </h3>
                  <p>
                     Select your best quotation from result.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>-->
<!-- Get Quotation Steps end-->
<!-- Search area 3 start -->
<div class="search-area-3 clearfix d-none ">
   <div class="container">
      <div class="inline-search-area-2 row">
         <form role="search" method="get" class="search-form" action="">
            <div class="search-col col-6">
               <input type="text" name="search" class="form-control has-icon b-radius" placeholder="What are you looking for?">
            </div>
            <div class="search-col col-6">
               <i class="fa fa-map-marker icon-append"></i>
               <input type="text" name="location" class="form-control has-icon b-radius" placeholder="Location">
            </div>
            <div class="search-col col-6">
               <select class="selectpicker search-fields" name="location">
                  <option>All Services</option>
                  <option>Architectural</option>
                  <option>Construction</option>
                  <option>Interior</option>
                  <option>Landscape</option>
                  <option>Renovation</option>
               </select>
            </div>
            <div class="search-col col-6">
               <button class="btn button-theme btn-search btn-block b-radius">
               <i class="fa fa-search"></i><strong>Find</strong>
               </button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- Search area 3 end -->

<!-- Listing item start -->
<div class="main-title listing-item content-area bg-grea-3" >
   <div class="container">
      <!-- Main title -->
      <div class="main-title pt-4 pb-4">
         <h2>
            Premium Plus Companies
         </h2>
         <p>
            Featured architects, builders, & designers at a glance:
         </p>
      </div>
     <div class="container">
     
    <hr>
  <div class="row">
<aside class="col-md-6">
          <p>Slider with pagination</p>
<!-- ================== 1-carousel bootstrap  ==================  -->

<div id="carousel1_indicator" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel1_indicator" data-slide-to="0" class="active"></li>
    <li data-target="#carousel1_indicator" data-slide-to="1"></li>
    <li data-target="#carousel1_indicator" data-slide-to="2"></li>
  </ol>  
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('public/assets/images/naqshalogo.jpg') }}"> 
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('public/assets/images/naqshalogo.jpg') }}">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('public/assets/images/naqshalogo.jpg') }}">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> 
<!-- ==================  1-carousel bootstrap ==================  .// -->   
  </aside> <!--<col.//>-->
  <aside class="col-md-6">
      <p>Slider with caption</p>
<!-- 2-carousel bootstrap -->
<div id="carousel2_indicator" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('public/assets/images/cover.jpg') }}">
      <article class="carousel-caption d-none d-md-block">
      <h5>First slide label</h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt.</p>
    </article> <!-- carousel-caption .// -->
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="" alt="Second slide">
      <article class="carousel-caption d-none d-md-block">
      <h5>Second slide label</h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt.</p>
    </article> <!-- carousel-caption .// -->
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="" alt="Third slide">
      <article class="carousel-caption d-none d-md-block">
      <h5>Third slide label</h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt</p>
    </article> <!-- carousel-caption .// -->
   </div>
  </div>
  <a class="carousel-control-prev" href="#carousel2_indicator" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel2_indicator" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> 
<!-- ==================  2-carousel bootstrap.// ==================  -->  
 </aside>
  </div>
</div>
   </div>
</div>

<!-- Listing item end -->
<style type="text/css">
</style>
<!-- Categories strat -->
<div class="categories content-area-7">
    <div class="container">
      <!-- Main title -->
      <div class="main-title text-center main-title-margin">
         <h1>Gallery</h1>
      </div>
      <div class="row">
        
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
              @if(isset($data['gallery_category']) && !empty($data['gallery_category']))
              @foreach($data['gallery_category'] as $key => $value)
              @if($key == 0)
               <div class="col-sm-12 col-pad">
                  <div class="category">
                     <a href="{{ route('gallery.3d.type',$value->id) }}">
                        <div class="category_bg_box category_bg_box_1" style="background: url('images/settings/{{ $value->gc_image }}') 0px 0px">
                           <div class="category-overlay">
                              <div class="category-content">
                                 <h3 class="category-title">
                                    {{ $value->gc_name }} 
                                 </h3>
                                 <h4 class="category-subtitle" id="con_count_id"></h4>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
               </div>
               @else
               <div class="col-lg-6 col-sm-6 col-pad">
                  <div class="category">
                     <a href="{{ route('gallery.3d.type',$value->id) }}">
                        <div class="category_bg_box lazy category_bg_box_2" style="background: url('images/settings/{{ $value->gc_image }}') 0px 0px">
                           <div class="category-overlay">
                              <div class="category-content">
                                 <h3 class="category-title">
                                    {{ $value->gc_name }} 
                                 </h3>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
               </div>
               @endif
              @endforeach
              @endif
            </div>
         </div>
        
        
      </div>
   </div>
</div>
<!-- Categories end -->

<!--Start Explore Market Place-->
<div class="container">
    <dic class="explore_marketplace">
        <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
               Explore The Marketplace
            </h2>
         </div>
         </div>
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href="#"><img src="public/assets/images/architecture.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Architecture</h3>
</a>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/interior_design.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Interior Design</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href="#"><img src="public/assets/images/Townplaner.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Town Planning</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/construction-icon-png-1.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Construction</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
               <a href=""><img src="public/assets/images/land.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Landscape</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/engineere.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Engineering</h3>
            </div>
        </div>
    </dic>
</div>
 
<!--End Exlore Market Place-->
<!--Registered companies-->
<div class="main-title partners mb-0 d-xs-block d-sm-block d-md-block d-lg-block d-xl-none" >
   <div class="container pt-4 pb-4">
      <div data-aos="fade-left"
         data-aos-duration="3000">
         <h1 class="pb-4 register-company">
            Registered Companies
         </h1>
         <div class="row">
            <div class="col-sm-3 col-xs-6 ">
               <a href="#">
               <b> Architecture</b>  
               <span class="arch_count_id"> </span>
               </a>
            </div>
            <div class="col-sm-3 col-xs-6 ">
               <a href="#"> 
               <b>Construction</b> 
               <span class="con_count_id"></span>
               </a>
            </div>
            <div class="col-sm-3 col-xs-6 ">
               <a href="#"> 
               <b>Interior</b> 
               <span class="inter_count_id"> </span>
               </a>
            </div>
            <div class="col-sm-3 col-xs-6 ">
               <a href="">     
               <b>Landscape</b> 
               <span class="land_count_id"> </span>
               </a>
            </div>
         </div>
      </div>
   </div>
</div>

<style type="text/css">
</style>
<div class="overview-bgi testimonial-2" style="">
   <div class="container">
      <div class="row">
         <div class="col-lg-8 offset-lg-2">
            <div class="testimonial-inner">
               <header class="testimonia-header">
                  <h1 class="pt-4">
                     What Our Customers Say
                  </h1>
               </header>
               <div class="carousel slide" id="carouselExampleIndicators7" data-ride="carousel">
                  <div class="carousel-inner">
                     <div class="carousel-item active carousel-item-left">
                        <div class="row">
                           <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4 col-xl-4">
                              <div class="avatar">
                                 <div id="bh_fb"></div>
                              </div>
                           </div>
                           <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 col-xl-8">
                              <h4 class="text-light">
                                 Three Digit Construction
                              </h4>
                              <p class="lead">
                                 The vision of this website is very creative.  Being registered on this company has promoted my number of clients.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item  carousel-item-next carousel-item-left">
                        <div class="row">
                           <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4 col-xl-4">
                              <div class="avatar">
                                 <div id="kb_fb"></div>
                              </div>
                           </div>
                           <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 col-xl-8">
                              <h4 class="text-light">
                                 Beenish Builders
                              </h4>
                              <p class="lead">
                                 I’ve found this innovative idea perfect for the market problem. This platform has increased my number of clients.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class="row">
                           <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4 col-xl-4">
                              <div class="avatar">
                                 <div id="aca_fb"></div>
                              </div>
                           </div>
                           <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 col-xl-8">
                              <h4 class="text-light">
                                 Hamara Ghar
                              </h4>
                              <p class="lead">
                                 I appreciate the manner in which the site is maintained. Naqsha Ghar has enhanced the name of my company and I’m getting more projects.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class="row">
                           <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4 col-xl-4">
                              <div class="avatar">
                                 <div id="ai_fb"></div>
                              </div>
                           </div>
                           <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 col-xl-8">
                              <h4 class="text-light">
                                  Sarbuland
                              </h4>
                              <p class="lead">
                                 The professional and practical approach of the services through a website has increased the communication with the clients.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class="row">
                           <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4 col-xl-4">
                              <div class="avatar">
                                 <div id="altec_eng"></div>
                              </div>
                           </div>
                           <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 col-xl-8">
                              <h4 class="text-light">
                                 ALTEC Engineers
                              </h4>
                              <p class="lead">
                                 I am very pleasantly surprised with the level of detail about this website and you have no idea how much time you have saved me.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <a href="#carouselExampleIndicators7" class="carousel-control-prev" data-slide="prev" role="button">
                  <span aria-hidden="true" class="slider-mover-left">
                  <i class="fa fa-angle-left"></i> 
                  </span>
                  </a>
                  <a href="#carouselExampleIndicators7" class="carousel-control-next" data-slide="next" role="button">
                  <span aria-hidden="true" class="slider-mover-right">
                  <i class="fa fa-angle-right"></i>
                  </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
   $(document).ready(function(){
     $('.customer-logos').slick({
         slidesToShow: 6,
         slidesToScroll: 1,
         autoplay: true,
         autoplaySpeed: 1500,
         arrows: false,
         dots: false,
         pauseOnHover: false,
         responsive: [{
             breakpoint: 768,
             settings: {
                 slidesToShow: 4
             }
         }, {
             breakpoint: 520,
             settings: {
                 slidesToShow: 3
             }
         }]
     });
   });
</script>
@endpush