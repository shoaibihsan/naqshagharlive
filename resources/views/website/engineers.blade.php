@extends('website.master-layout')

@push('css')

@endpush

@section('content')
<div class=" overview-bgi d-print-none" style="background-image:url(https://www.naqsha.com.pk/wp-content/themes/naqsha/img/cover/cons.jpg );">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
            <!-- construction Form -->
            <form method="post" id="landscape_form">
               <div class="submit-address dashboard-list">
                  <div class="row">
                     <h3 class="text-center">Engineering</h3>
                  </div>
                  @include('website.includes.search_filter_form')
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="clearfix visible-xs"></div>
</div>
<!--Start -->
<div class="container">
        <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
                Explore More Construction Services
            </h2>
         </div>
         </div>
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href="#"><img src="public/assets/images/architecture.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Architecture</h3>
</a>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/interior_design.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Interior Design</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href="#"><img src="public/assets/images/Townplaner.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Town Planning</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/construction-icon-png-1.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Construction</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
               <a href=""><img src="public/assets/images/land.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Landscape</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/engineere.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Engineering</h3>
            </div>
        </div>
    </dic>
</div>
 
<!--End Exlore Market Place-->
<!--Start The FAQ of Construction-->
<div class="container">
    <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
                Engineering FAQs
            </h2>
         </div>
         </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
            <h6>What is Engineering?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
            <h6>What is Interior?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results.</p>
            <h6>What is Interior?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
        </div>
         <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
            <h6>What is Interior?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
            <h6>What is Interior?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results.</p>
            <h6>What is Interior?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
        </div>
    </div>
</div>
<!--End FAQ-->


@include('website.includes.image_popup_models')
@endsection

@push('script')

@endpush