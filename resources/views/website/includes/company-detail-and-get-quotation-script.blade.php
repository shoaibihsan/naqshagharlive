<script type="text/javascript">
    function detail_get(id, url, name, type, size, rate) {
        var floor = '';
        $('#myModaldetail').modal('show');
        floor = jQuery('#construction_type').val();
        jQuery(".page_loader").fadeIn("fast");
        jQuery('#detail_table_view').empty();
        jQuery('#detail_plot_type').empty();
        jQuery('#arc_res_DS').empty();
        jQuery('#view_profile_company').empty();
        jQuery('#company_name_detail').empty();
        jQuery('#company_name_detail').append(name);
        jQuery('#detail_plot_type').append(jQuery('#plot_size option:selected').text() + ' ' + jQuery('#project_type option:selected').text());
        if (jQuery('#project_type').val() == 'arc_res_') {

            jQuery('#detail_table_view').append('<tr style=" background-color: rgba(0,0,0,.05);"> <th>Type</th> <th> ' + jQuery('#construction_type option:selected').text() + ' </th></tr><tr> <th>Package Fee</th> <td> Rs. ' + rate + '</td></tr>');
            jQuery('#arc_res_DS').append('<strong>List of Basic Drawing Set</strong><ol><li>Planning and designing (design concept).<li>Front elevation design (3D presentation).<li>Submission drawings with 5 blue prints for approval.<li>Foundation working.<li>Working plans.<li>Selection details.<li>Elevation working.<li>Electric layouts.<li>Sanitary & sewerage layouts.<li>Structural layouts.<li>Roof plan.</ol>');
            jQuery('#view_profile_company').attr("href", 'https://www.naqsha.com.pk/services/' + url);
        } else {

            jQuery('#detail_table_view').append('<tr style=" background-color: rgba(0,0,0,.05);"> <th>Type</th> <th> ' + jQuery('#construction_type option:selected').text() + ' </th></tr><tr> <th>Package Fee</th> <td> Rs. ' + rate + '</td></tr>');
            jQuery('#arc_res_DS').append('<strong>List of Basic Drawing Set</strong><ol><li>Planning and designing (design concept).<li>Front elevation design (3D presentation).<li>Submission drawings with 5 blue prints for approval.<li>Foundation working.<li>Working plans.<li>Selection details.<li>Elevation working.<li>Electric layouts.<li>Sanitary & sewerage layouts.<li>Structural layouts.<li>Roof plan.</ol>');
            jQuery('#view_profile_company').attr("href", 'https://www.naqsha.com.pk/services/' + url);
        }
    }

    function send_company_id(id, name, rate) {
        jQuery(".page_loader").fadeIn("fast");
        company_name = name;
        company_id = id;
        company_rate = rate;
        $("#company_id").val(company_id);
        $("#company_name").val(company_name);
        $("#company_rate").val(company_rate);
        
    // data-toggle="modal" data-target="#myModal"
    $('#myModal').modal('show');
    //  $('#myModal').modal({
    //     show: 'true'
    // });
        jQuery('#table_quote').empty();
        jQuery('#table_quote').hide();
        jQuery('#table_quote').append('<tr style=" background-color: rgba(0,0,0,.05);"> <th>Company Name</th> <td>' + company_name + '</td></tr><tr> <th>Plot Location</th> <td>' + jQuery('#plot_location option:selected').text() + '</td></tr><tr style=" background-color: rgba(0,0,0,.05);"> <th>Professional Lahore</th> <td>' + jQuery('#professional_location option:selected').text() + '</td></tr><tr> <th>Type</th> <td>' + jQuery('#project_type option:selected').text() + '</td></tr><tr style=" background-color: rgba(0,0,0,.05);"> <th>Plot Size</th> <td>' + jQuery('#plot_size option:selected').text() + '</td></tr><tr> <th>Rate</th> <td> Rs. ' + company_rate + '</td></tr>');
        jQuery(".page_loader").fadeOut("fast");
    }
</script>