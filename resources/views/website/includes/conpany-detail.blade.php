<div class="modal d-print-none" id="myModaldetail">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3 class="modal-title" id="company_name_detail"></h3>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- Modal body -->
            <div class="widget booking-now  d-xl-block d-lg-block" style="overflow-y: auto; margin-bottom: 0 !important;">
                <div style="overflow: auto; max-height: 300px;">
                    <h4>Drawing Detail</h4>
                    <p id="arc_res_DS"></p>
                </div>
                <h4>Rates for <span id="detail_plot_type"></span></h4>
                <table class="table table-striped" id="detail_table_view">
                </table>
                <!-- Modal footer -->
                <div class="form-group mb-0">
                    <a id="view_profile_company" href="#">
                        <button class="search-button btn btn-warning" type="button">View Profile</button>
                    </a>
                </div>
            </div>
            
        </div>
    </div>
</div>