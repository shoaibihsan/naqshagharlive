<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg">
   <head>
       <link rel="icon" href="public/assets/images/fivicon.png" type="image/png" sizes="32x32">
      <title>Apna Ghar Apna Naqsha - Perfect Constructional
Services For Your Buildings - Naqshaghar.com </title>

      <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/styles.css') }}">
      <link rel="stylesheet" href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
      <!-- Favicon icon -->
      <link rel="shortcut icon" href="" type="image/x-icon" >
      <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/custom.css') }} ">
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
      @stack('css')
   </head>
   <body >
      <!--<div class="page_loader"></div>-->
      <div class="container-fulid container-home-page">
         <div class="row container-home-page-row" style="">
            <div class="col-md-6 d-none d-sm-none d-md-block  text-left text-color-white">
               <a href="http://www.facebook.com/NaqshaGhar.pk" target="_blank" class="facebook facebook-icon-color text-color-white">
                  <i class="fa fa-facebook"></i>
               </a>
               <a href="" target="_blank" class="twitter facebook-icon-color text-color-white">
                  <i class="fa fa-twitter"></i>
               </a>
               <a href="" target="_blank" class="google facebook-icon-color text-color-white">
                  <i class="fa fa-instagram"></i>
               </a>
               <a href="" target="_blank" class="linkedin facebook-icon-color text-color-white">
                  <i class="fa fa-linkedin"></i>
               </a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 text-right text-color-white">
                  For Registration Call us 
               <a href="" class="text-color-white">
                 +92-300-8555550
               </a>
            </div>
         </div>
      </div>