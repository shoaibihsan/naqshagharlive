


      <footer class="footer">
         <div class="container footer-inner">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-right" data-aos-duration="4000">
                  <h4 class="footer-h4">
                     About Us
                  </h4>
                  <hr class="footer-hr">
                  <div class="footer-item clearfix">
                     
                     <div class="text">
                        <p class="footer-p-text">
                          NQSHA GHAR is a company which provides construction industry in Pakistan. It brings professionals and clients together. We sum up all experts on website like Architects, Engineers, Designers and Developers. We deal with all main cities including LHR, KHI, ISD, RWP, SKT, FSD,GWA, MTN and SPR in Pakistan.
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="4000">
                  <div class="offset-lg-2 col-lg-8 offset-lg-2 col-sm-12 padding-left-0">
                     <h4 class="footer-h4 h4-center">
                        Quick Links
                     </h4>
                     <hr class="footer-hr hr-center">
                  </div>
                  <div class="row">
                     <div class="offset-xl-2 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="footer-item">
                           <ul class="links">
                              <li>
                                 <a href="{{url('/')}}">&#8250; Home</a>
                              </li>
                              <li>
                                 <a href="about-us">&#8250; About Us</a>
                              </li>
                              <li>
                                 <a href="terms">&#8250; Terms & Conditions</a>
                              </li>
                              <li>
                                 <a href="faq">&#8250; FAQs</a>
                              </li>
                              <li>
                                 <a href="blogs">&#8250; Blogs</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="offset-xl-1 col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="footer-item">
                           <ul class="links">
                              <li>
                                 <a href="architecture">&#8250; Architecture</a>
                              </li>
                              <li>
                                 <a href="construction">&#8250; Construction</a>
                              </li>
                              <li>
                                 <a href="interior">&#8250; Interior</a>
                              </li>
                              <li>
                                 <a href="landscape">&#8250; Landscape</a>
                              </li>
                              <li>
                                 <a href="town-planner">&#8250; Town Planning</a>
                              </li>
                              <li>
                                 <a href="engineers">&#8250; Engineering</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-left" data-aos-duration="4000">
                  <div class="footer-item" id="contact_us">
                     <h4 class="footer-h4">
                        Contact Us
                     </h4>
                     <hr class="footer-hr">
                     <ul class="contact-info">
                        <li style="">
                           <i class="fa fa-map-marker"></i>
                           Office #51-MB 4th Floor DHA phase 6 Lahore.
                        </li>
                      
                        <li>
                           <i class="fa fa-envelope-o"></i>
                           <a href="">
                              info@naqshaghar.com
                           </a>
                        </li>
                        <li>
                           <i class="fa fa-mobile fa-2x"></i>
                           <a href="tel:+92-300-8555550">
                              +92-300-8555550
                           </a>
                        </li>
                        <li>
                           <i class="fa fa-mobile fa-2x" ></i>
                              <a href="tel:+92-728-88888">
                                 +92-300-7288888 Help Line
                              </a>
                        </li>
                      
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- Footer end -->
      <!-- Sub footer start -->
      <div class="sub-footer">
         <div class="container">
            <div class="row ">
                <img src="{{ asset('public/assets/images/Header_logo.png') }}" alt="logo" class="f-logo">
               <div class="col-lg-4 col-md-4">
                   
                  <p class="copy">
                     © 
                     <a href="/">
                        Three Digit Pvt Ltd 2020
                     </a>
                  </p>
               </div>
               <div class="col-lg-4 col-md-6 col-sm-6">
                  <ul class="social-list clearfix text-center footer-social-list">
                     <li>
                        <a href="http://www.facebook.com/NaqshaGhar.pk" target="_blank"  class="facebook">
                           <i class="fa fa-facebook text-color-white"></i>
                        </a>
                     </li>
                     <li>
                        <a href="#" target="_blank" class="twitter">
                           <i class="fa fa-twitter text-color-white"></i>
                        </a>
                     </li>
                     <li>
                        <a href="" target="_blank" class="google">
                           <i class="fa fa-instagram text-color-white"></i>
                        </a>
                     </li>
                     <li><a href="" target="_blank" class="linkedin">
                        <i class="fa fa-linkedin text-color-white"></i>
                     </a>
                  </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Sub footer end -->
      <!-- Full Page Search -->
      <div id="full-page-search">
         <button type="button" class="close">×</button>
         <form action="">
            <input type="search" value="" placeholder="type keyword(s) here" />
            <button type="submit" class="btn btn-sm button-theme">Search</button>
         </form>
      </div>
      <a id="page_scroller" href="#top">
         <i class="fa fa-chevron-up"></i>
      </a>
      <a href="https://api.whatsapp.com/send?phone=923008555550&text=I+want+to+get+registered+with+http://naqshaghar.threedigit.com/" class="whatsapp-icons">
         <img src="" width='60px'>
      </a>
      <script>
         var logo_url="https://www.naqshaghar.com";
      </script>

      <script src="{{ asset('public/assets/js/jquery-2.2.0.min.js') }}"></script>
      <script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
      <script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
      <script defer src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
      <script defer src="{{ asset('public/assets/js/jquery.scrollUp.js') }}"></script>
      <script defer src="{{ asset('public/assets/js/slick.min.js') }}"></script>
      <script defer src="{{ asset('public/assets/js/app.min.js') }}"></script>
      <script defer src="{{ asset('public/assets/js/datatables.min.js') }}"></script>
      <script defer src="{{ asset('public/assets/js/select2.min.js') }}"></script>
      <script defer  src="{{ asset('public/assets/js/ie10-viewport-bug-workaround.js') }}"></script>
      <script  src="{{ asset('public/assets/js/ie10-viewport-bug-workaround.js') }}"></script>
      <!-- //datatable -->
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
      <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>   
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script type="text/javascript">
   @if(Session::has('notification'))
    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;
    }
  @endif
</script>


  @stack('script')

      <script type="text/javascript">
    $(document).ready(function() {
        $('#architecture').DataTable();
    });
  </script>
      <script>
         AOS.init({
         duration: 1200,
         disable: 'mobile'
         });
         
      </script>
      <script>
         $(document).ready(function () {
           $('.lazy').lazy({
                     delay: 2000,
                 });
         
             
         $('.select2').select2();
                         $('#dismiss, .overlay').on('click', function () {
                             $('#sidebar').removeClass('active');
                             $('.overlay').removeClass('active');
                         });
         
                         $('#sidebarCollapse').on('click', function () {
                             $('#sidebar').addClass('active');
                             $('.overlay').addClass('active');
                             $('.collapse.in').toggleClass('in');
                             $('a[aria-expanded=true]').attr('aria-expanded', 'false');
             });
         
          
             
             $('#projectType').on('change',function(){ 
                 var value = $(this).val();
                 
                 if(value == 'commercial') {    
                     $('#ConstructionType option[value="double_story"]').remove();
                     $('#ConstructionType').append('<option value="floors" >No of Floors (1-12)</option>');
                 }else if(value == 'residential') {
                     $('#ConstructionType option[value="floors"]').remove();
                     $('#ConstructionType').append('<option value="double_story" >Double Story</option>');
                 }
             });
         
            var loc = window.location.origin+window.location.pathname;
            
         
             $(".bi-4 .banner-info ul li").on( "click", function() {
         
             $(".bi-4 .banner-info ul li a").removeClass("home-cat");
            $(".bi-4 .banner-info ul li a h5").removeAttr("style");
         
             //$('a',this).css('background-color','#FFD400');
             $('a',this).addClass("home-cat");
            
         
            $('a.home-cat h5',this).css({color:"black"});
         
         });
             
            setTimeout(function(){
               if($('#noticepopup').length){
                    $('#noticepopup').modal('show');
               }
            }, 500);  
         });
         
         function select_my_city(name){
         $('#nameofcity').empty();
             $('#nameofcity').append(name);
         $('#SelectCity').modal('hide')
         }
         function select_my_area(name){
         $('#nameofarea').empty();
             $('#nameofarea').append(name);
         $('#SelectArea').modal('hide')
         }
          $('#btn-how-it-work').click(function() {
                         $('html, body').animate({
                             scrollTop: $("#how-it-works").offset().top
                         }, 1000);
          });
          $('#btn-contact-us').click(function() {
                         $('html, body').animate({
                             scrollTop: $("#contact_us").offset().top
                         }, 1000);
          });
          $('#btn-how-it-work-mobi').click(function() {
                         $('html, body').animate({
                             scrollTop: $("#how-it-works").offset().top
                         }, 1000);
                         $('#sidebar').removeClass('active');
                             $('.overlay').removeClass('active');
          });
          $('#btn-contact-us-mobi').click(function() {
                         $('html, body').animate({
                             scrollTop: $("#contact_us").offset().top
                         }, 1000);
                         $('#sidebar').removeClass('active');
                             $('.overlay').removeClass('active');
         
                             
          });
      </script>
   </body>
</html>
<a href="https://api.whatsapp.com/send?phone=923008555550&text=I+want+to+registered+with+naqsha.com.pk"  style='    position: fixed;
   bottom: 79px;
   right: 20px;'><img src="" width='60px'></a>
<script  defer>
   jQuery(document).ready(function($) {
      // Your code in here
    $(".page_loader").fadeOut("fast");
   jQuery.ajax({
              url: '',
              type: "POST",
              dataType: "json",
              data: {'data':'data'},
              success: function(data) {
                  //
                  var temp = data;
                  console.log(data);
                  // console.log(data.data[0].title);
                   jQuery('.arch_count_id').html(data.data['arch_posts']+' listings');
                  jQuery('.con_count_id').html(data.data['con_posts']+' listings');
                  jQuery('.inter_count_id').html(data.data['inter_posts']+' listings');
                  jQuery('.land_count_id').html(data.data['land_posts']+' listings');
              },
              error: function(data) {
                  console.log(data);
              }
          });
   });
   var lastScrollTop = 0;
   var loadhowitwork=false;
   var loadconnectiveservice=false;
   
   
</script>

</body>
</html>