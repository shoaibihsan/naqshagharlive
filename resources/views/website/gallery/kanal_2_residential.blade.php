@extends('website.master-layout')

@push('css')

@endpush


@php 
    $banner_image = isset($gimages[0]['galllerySubCategory']['image_name']) ? $gimages[0]['galllerySubCategory']['image_name'] : '';
@endphp

@section('content')
	@include('website.includes.header_banners.aboutus-header')
	<div class="overview-bgi listing-banner" style="background-image:url('../images/settings/{{ $banner_image }} ');background-size: 100% 100%;">
    <div class="container listing-banner-info">
        <div class="row">
            <div class="col-lg-7 col-md-12 clearfix">
                <div class="text">
                    <h1> Residential </h1>
                   
                 
                </div>
            </div>
            <div class="col-lg-5 col-md-12 clearfix">
                <div class="cover-buttons">
                    <ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="listing-details-page content-area-6" style="padding-top:40px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!-- listing description start -->
              


                
                <!-- gallery start -->
                <div class="gallery">
                    <h3 class="heading-2">
                    Gallery
                    </h3>
                    <div class="container">
                        <div class="row">
                            @if(isset($gimages) && !empty($gimages))
                            @foreach($gimages as $key => $value)                              
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="{{ asset('images/settings'.'/'.$value->image_name)}}" data-lightbox="roadtrip">
                                     <img src="{{ asset('images/settings'.'/'.$value->image_name)}}" alt="{{ $value->name }}" class="img-fluid">
                                      </a>
                                </div>
                            @endforeach
                            @endif

                                
                                                                
                             {{--  <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/10/Hunains-Architect-Top-10-Architect-Lahore-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/10/Hunains-Architect-Top-10-Architect-Lahore-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan.jpg" alt="Hunain's-Architect-Top-10-Architect-Lahore,-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/10/Hunains-Architect-Top-10-Architect-Lahore-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/10/Hunains-Architect-Top-10-Architect-Lahore-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan-2.jpg" alt="Hunain's-Architect-Top-10-Architect-Lahore,-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan-2" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/10/Hunains-Architect-Top-10-Architect-Lahore-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan-4.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/10/Hunains-Architect-Top-10-Architect-Lahore-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan-4.jpg" alt="Hunain's-Architect-Top-10-Architect-Lahore,-Top-10-Architect-Pakistan-Naqsha-Lahore-Pakistan-4" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Tameer-karo-Builders-Lahore-Pakistan-Naqsha-6.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Tameer-karo-Builders-Lahore-Pakistan-Naqsha-6.jpg" alt="Tameer-karo-Builders-Lahore-Pakistan-Naqsha-6" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-1.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-1.jpg" alt="Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-1" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-3.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-3.jpg" alt="Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-3" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-4.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-4.jpg" alt="Creative-Assocites-Builders-Archietcs-Gujranwala-Pakistan-Naqsha-4" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Tabeer-Construction-Gujranwala-Pakistan-Naqsha-1.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Tabeer-Construction-Gujranwala-Pakistan-Naqsha-1.jpg" alt="Tabeer-Construction-Gujranwala-Pakistan-Naqsha-1" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Tabeer-Construction-Gujranwala-Pakistan-Naqsha-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Tabeer-Construction-Gujranwala-Pakistan-Naqsha-2.jpg" alt="Tabeer-Construction-Gujranwala-Pakistan-Naqsha-2" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-1.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-1.jpg" alt="AM-Builders-Karachi-Pakistan-Naqsha-1" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-5.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-5.jpg" alt="AM-Builders-Karachi-Pakistan-Naqsha-5" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-4.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-4.jpg" alt="AM-Builders-Karachi-Pakistan-Naqsha-4" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-3.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/AM-Builders-Karachi-Pakistan-Naqsha-3.jpg" alt="AM-Builders-Karachi-Pakistan-Naqsha-3" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Ettihad-BUilders.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Ettihad-BUilders.jpg" alt="Ettihad-BUilders" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Ettihad-Builders-5.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Ettihad-Builders-5.jpg" alt="Ettihad-Builders-5" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Ettihad-Builders-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/Ettihad-Builders-2.jpg" alt="Ettihad-Builders-2" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/Konsult-Konstruct-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/Konsult-Konstruct-2.jpg" alt="Konsult-&amp;-Konstruct-2" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/Konsult-Konstruct-1.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/Konsult-Konstruct-1.jpg" alt="Konsult-&amp;-Konstruct-1" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/Konsult-Konstruct-5.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/Konsult-Konstruct-5.jpg" alt="Konsult-&amp;-Konstruct-5" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-15.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-15.jpg" alt="2-kanal-bh-consultants-15" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-14.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-14.jpg" alt="2-kanal-bh-consultants-14" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-13.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-13.jpg" alt="2-kanal-bh-consultants-13" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-12.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-12.jpg" alt="2-kanal-bh2 kanal residential house 3d elevation pakistan naqsha-consultants-12" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-11.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-11.jpg" alt="2-kanal-bh-consultants-11" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-10.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-10.jpg" alt="2-kanal-bh-consultants-10" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-9.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-9.jpg" alt="2-kanal-bh-consultants-9" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-8.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-8.jpg" alt="2-kanal-bh-consultants-8" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-7.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-7.jpg" alt="2-kanal-bh-consultants-7" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-6.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-6.jpg" alt="2-kanal-bh-consultants-6" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-5.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-5.jpg" alt="2-kanal-bh-consultants-5" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-4.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-4.jpg" alt="2-kanal-bh-consultants-4" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-3.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-3.jpg" alt="2-kanal-bh-consultants-3" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-2.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-2.jpg" alt="2-kanal-bh-consultants-2" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                              <div class="col-3 mb-4 aos-init" data-aos="flip-left">                                     
                                <a href="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-1.jpg" data-lightbox="roadtrip">
                                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/2-kanal-bh-consultants-1.jpg" alt="2-kanal-bh-consultants-1" class="img-fluid">
                                      </a>
                                </div>

                                
                                                                
                           </div> --}}
                           
                    </div> 
                </div>
                <!-- Location start
                
                                
                            </div>
            <!-- Services -->
           
				</div>
            </div>
        </div>
    </div>

@endsection

@push('script')

@endpush