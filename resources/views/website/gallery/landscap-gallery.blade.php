@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	@include('website.includes.header_banners.aboutus-header')

	<div class="overview-bgi listing-banner" style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/cover/arch.jpg ');">
    <div class="container listing-banner-info">
        <div class="row">
            <div class="col-lg-12 col-md-12 clearfix">
                <div class="text">
                    <h1> Gallary Type </h1>
                   
                </div>
            </div>
            <div class="col-lg-5 col-md-12 clearfix">
                <div class="cover-buttons">
                    <ul>
                      
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="listing-section content-area d-print-none" style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/bg_cover.png ');">
	<div class="container">
		<div class="row">
			<div class="col">
               <div class="row">
                    
              
				      <div class="blog-1 col-4 aos-init" data-aos="flip-left">
                    <div class="blog-photo">
                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/8-1.jpg" alt="small-blog" class="img-fluid">
                       
                       <!--  <div class="date-box">
                            <span>14</span>Nov
                        </div> -->
                    </div>
                    <div class="detail">
                        <h3>
                            <a href="{{ route('landscapes') }}">Landscape</a>
                        </h3>
                       
                        <a href="{{ route('landscapes') }}" class="read-more">View more</a>
                    </div>
                </div>


     </div>

		   </div>
		</div>
	</div>
</div>
@endsection

@push('script')

@endpush