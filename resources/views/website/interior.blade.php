@extends('website.master-layout')

@push('css')
<style type="text/css">
    html {
    font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
    font-size: 14px;
}

h5 {
    font-size: 1.28571429em;
    font-weight: 700;
    line-height: 1.2857em;
    margin: 0;
}

.card {
    font-size: 1em;
    overflow: hidden;
    padding: 0;
    border: none;
    border-radius: .28571429rem;
    box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
}

.card-block {
    font-size: 1em;
    position: relative;
    margin: 0;
    padding: 1em;
    border: none;
    border-top: 1px solid rgba(34, 36, 38, .1);
    box-shadow: none;
}

.card-img-top {
    display: block;
    width: 100%;
    height: auto;
}

.card-title {
    font-size: 1.28571429em;
    font-weight: 700;
    line-height: 1.2857em;
}

.card-text {
    clear: both;
    margin-top: .5em;
    color: rgba(0, 0, 0, .68);
}

.card-footer {
    font-size: 1em;
    position: static;
    top: 0;
    left: 0;
    max-width: 100%;
    padding: .75em 1em;
    color: rgba(0, 0, 0, .4);
    border-top: 1px solid rgba(0, 0, 0, .05) !important;
    background: #fff;
}

.card-inverse .btn {
    border: 1px solid rgba(0, 0, 0, .05);
}

.profile {
    position: absolute;
    top: -12px;
    display: inline-block;
    overflow: hidden;
    box-sizing: border-box;
    width: 25px;
    height: 25px;
    margin: 0;
    border: 1px solid #fff;
    border-radius: 50%;
}

.profile-avatar {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
}

.profile-inline {
    position: relative;
    top: 0;
    display: inline-block;
}

.profile-inline ~ .card-title {
    display: inline-block;
    margin-left: 4px;
    vertical-align: top;
}

.text-bold {
    font-weight: 700;
}

.meta {
    font-size: 1em;
    color: rgba(0, 0, 0, .4);
}

.meta a {
    text-decoration: none;
    color: rgba(0, 0, 0, .4);
}

.meta a:hover {
    color: rgba(0, 0, 0, .87);
}
.company-image{
    width: 297px !important;
    height: 178.19px !important;
}
</style>
@endpush

@section('content')

<div class=" overview-bgi d-print-none" style="background-image:url('../../assets/images/interior.jpg' );">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
            <!-- construction Form -->
           
               <div class="submit-address dashboard-list">
               
                  <div class="row">
                     <h3 class="text-center">Interior</h3>
                     <h5 style="padding-left: 30px;
                        color: white;">
                        Select the options given below 
                     </h5>
                  </div>
                  @include('website.includes.search_filter_form')
               </div>
          
         </div>
      </div>
   </div>
   <div class="clearfix visible-xs"></div>
</div>

<div class="container main_head"><p style="padding:5px;font-size:15px;"><a href"#"> Naqsha Ghar</a>  > <a href"#"> Interior  ></a><h1 class="heading_construction">Buildings Interior</h1></p><h6 style="line-height: 0px;">Do you want to Interior Services for your building? Select the Interior company and get qoutation.</h6><div class="con_buttons"><p>
<button>Interior Options</button> <button>Type</button> <button>Rate Sqft</button> <button>Locations</button>  <button>Package</button></p></div>
<h6 style="margin-top:30px;">({{$data['in']}}) Services Available</h6>
</div>
<div class="row card_row">

            @foreach($cc as $index => $value)

            <div class="col-sm-4 col-md-4 col-lg-3 mt-4">
                <div class="card"><a  target="_blank" href="{{ route('company.detail',$value->company_id) }}">
<img class="card-img-top  company-image" src="{{ asset('images/company_profile'.'/'.$value->company['c_profile_image']) }}" width="250px" height="150px">
                    <div class="card-block">
                        <P>{{ $value->company['c_name'] }}
                                    
                                    </a></P>
                        <p>
                            <span>
                                  <i class="fa fa-map-marker location" aria-hidden="true"></i>

                            </span>
                            <span>{{$value->plot_location}}</span>
                          
                        </p>
                    <p>
                         <span>
                             
                         </span>
                         <span>
                       {{$value->details}} </span></p>
                   
                       <!-- <p>
                          <span>
                            Covered area:
                          </span>
                          <span>
                            {{$value->covered_area}}
                          </span>
                        </p>-->
                        <p>
                          <span>
                            Type:
                          </span>
                          <span>
                            {{$value->plot_type}}
                          </span>
                        </p>
                        <p ><span>Rate Rs: </span><span style="font-weight: 500;">{{$value->rate_sqft}} (Per Sqft)</span></p>

                    </div>
                                           <div class="card-footer">
                    <div class="row" style="padding-left: 0px;">
                        <div class="col-sm-6" style="padding-left: 2px;">
                           <div class="heart_icon new_heart"><i class="fa fa-heart" aria-hidden="true"></i></div> 
                 <!-- <button class="btn btn-secondary float-left ">Rate/Sqft
                          {{$value->rate_sqft}}</button>--></div>
                        <div class="col-md-6">
                            
                       
                         <button type="button" class="btn btn-warning" onclick="send_company_id('{{ $value->company_id }}','{{ $value->company['c_name']}}','{{ $value->rate_sqft }}');">Get Quotation</button>
                        </div>
                     </div>
                        
                    </div>
                  </div>
                </div>
                    @endforeach
                </div>
                     <div class="col-md-12 text-right">
                    {{ $cc->links() }}
                </div>
   
<!--Start Explore Market Place-->
<div class="container-fluid explore_services">
        <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
                Explore More Construction Services
            </h2>
         </div>
         </div>
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href="#"><img src="public/assets/images/architecture.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Architecture</h3>
</a>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/interior_design.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Interior Design</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href="#"><img src="public/assets/images/Townplaner.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Town Planning</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/construction-icon-png-1.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Construction</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
               <a href=""><img src="public/assets/images/land.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Landscape</h3>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
                <a href=""><img src="public/assets/images/engineere.png" alt="Smiley face" height="80" width="150" style="border-style: none;display: block;margin-left: auto;margin-right: auto;width: 50%;">
                <h3 style="font-family: Open Sans;font-weight: ;font-size: 20px;line-height: 24px;color: #000000; ;margin-bottom: 20px;text-align:center">Engineering</h3>
            </div>
        </div>
    </dic>
</div>
 
<!--End Exlore Market Place-->
<!--Start The FAQ of Construction-->
<div class="container-fluid">
    <div class="main-title services content-area">
         <div class="text-center pt-4 pb-4">
            <h2>
                Construction FAQs
            </h2>
         </div>
         </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
            <h6>What is Construction?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
            <h6>What is Construction?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results.</p>
            <h6>What is Construction?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
        </div>
         <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
            <h6>What is Construction?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
            <h6>What is Construction?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results.</p>
            <h6>What is Construction?</h6>
            <p>SEO means "search engine optimization" and it's a crucial part of your overall online strategy. It’s about improving your online visibility by achieving high rankings on Google search results and attract organic search traffic. There are several SEO services you can buy to help you boost your online presence. These include on-page optimization such as keyword research and meta tags, off-page optimization to enhance online authority with quality content and link building, and technical improvements to help your site achieve more prominence in search results
            </p>
        </div>
    </div>
</div>


<!--End FAQ-->


@include('website.includes.conpany-detail') 
@include('website.includes.get-quotation')

@include('website.includes.google_recaptcha')

@endsection

@push('script')
  @include('website.includes.company-detail-and-get-quotation-script')
@endpush