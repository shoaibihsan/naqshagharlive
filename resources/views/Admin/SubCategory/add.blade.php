@extends('Admin.admin-layout') @push('css') @endpush @section('content')

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="widget red">
                <div class="widget-title">
                    <h4><i class=" icon-key"></i>Gallery </h4>
                    <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    @if(isset($gc) && !empty($gc))
                    <form class="form-horizontal" method="post" action="{{ route('gallery.update',$gc->id) }} " enctype="multipart/form-data">
                        @method('put') @else
                        <form class="form-horizontal" method="post" action="{{ route('gallery.store') }}" enctype="multipart/form-data">
                            @endif @csrf
                            
                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">gc_name</label>
                                    <div class="controls">
                                        <input type="text" name="gc_name" id="name" class="form-control" @if(isset($gc) && !empty($gc)) value="{{ $gc->gc_name }}" @endif>
                                    </div>
                                </div>
                                   <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">gc_image</label>
                                    <div class="controls">
                                        <input type="file" name="gc_image" id="gc_image" class="form-control">
                                    </div>
                                </div>
                             
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success">
                                        @if(isset($gallery) && !empty($gallery)) Update @else Save @endif
                                    </button>
                                    <button type="button" class="btn">Cancel</button>
                                </div>
                        </form>
                        <!-- END FORM-->
                        </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>

@endsection 
@push('js') 

@endpush


