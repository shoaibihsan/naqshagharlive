@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->   
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN THEME CUSTOMIZER-->
      
         <!-- END THEME CUSTOMIZER-->
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         <h3 class="page-title">
            Gallery Subcategory
         </h3>
        
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN EDITABLE TABLE widget-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE widget-->
         <div class="widget purple">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i> Gallery Subcategory</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body">
               <div>
                  <div class="clearfix">
                     <div class="btn-group">
                        <a href="{{ route('gallarysub.create') }}" id="editable-sample_new" class="btn green">
                        Add New <i class="icon-plus"></i>
                        </a>
                     </div>
                 
                  </div>
                  <div class="space15"></div>
                  <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                    
                     <table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
                        <thead>

                           <tr role="row">
                              <th>#</th>
                              <th >name</th>
                              <th>Gallery Category</th>
                              <th>Image</th>
                              <th>Edit</th>
                              <th>Delete</th>
                           </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                           @foreach($gsc as $index => $value)
                        
                           <tr class="odd">
                              <td>{{ ++$index }}</td>
                              <td class="  sorting_1">{{ $value->name }}</td>
                              <td class="  sorting_1">{{ $value->gallleryCategory['gc_name'] }}</td>
                              <td class="  sorting_1">
                                 <img src="{{ asset('images/settings'.'/'.$value->image_name) }}" style="width: 100px; height: 100px">
                              </td>
                              
                              <td class=" "><a class="edit" href="{{ route('gallarysub.edit',$value->id) }}">Edit</a></td>
                              <td class=" ">
                                 <a class="delete" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-gallarysub-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-gallarysub-{{ $value->id }}" action="{{ route('gallarysub.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>
                              </td>
                           </tr>
                           @endforeach
                        
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!-- END EXAMPLE TABLE widget-->
      </div>
   </div>
   <!-- END EDITABLE TABLE widget-->
</div>
@endsection

@push('js')

@endpush