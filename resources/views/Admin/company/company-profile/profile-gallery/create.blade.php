@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')

<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
<br><br>   

   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN VALIDATION STATES-->
         <div class="widget red">
            <div class="widget-title">
               <h4><i class=" icon-key"></i> Company Profile Gallery  </h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body form">
               <!-- BEGIN FORM-->
               @isset($company_profile_gallery)
               <form  class="form-horizontal" method="post" action="{{ route('company-profile-gallery.update',[$company_profile_gallery->id]) }}" enctype="multipart/form-data">
                  @method('PUT')
               @else
               <form  class="form-horizontal" method="post" action="{{ route('company-profile-gallery.store') }}" enctype="multipart/form-data">
               @endisset   
                  @csrf
                  <div class="control-group success">
                     <label class="control-label" for="company_profile_id">Company Profile  </label>
                     <div class="controls">
                        <select class="form-control span6" id="company_profile_id" name="company_profile_id" required>
                              <option value="">Please Select Company Profile</option>
                           @foreach($companyprofile as $value)
                              <option value="{{ $value->id }}">{{ $value->cp_about_us }}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="cp_gallery_image_name">Company profile Image Name</label>
                     <div class="controls">
                        <input type="text" class="span6" id="cp_gallery_image_name" name="cp_gallery_image_name" required="" @isset($company_profile_gallery) value="{{ $company_profile_gallery->cp_gallery_image_name }}" @endisset>
                  
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="cp_gallery_image">Company profile Image</label>
                     <div class="controls">
                        <input type="file" class="span6" id="cp_gallery_image" name="cp_gallery_image" required="">
                       
                     </div>
                  </div>
                   
                  
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">
                        @isset($company_profile_gallery)
                           Update
                        @else
                        Save
                        @endisset
                     </button>
                     <button type="button" class="btn">Cancel</button>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
         <!-- END VALIDATION STATES-->
      </div>
   </div>

</div>
@endsection

@push('js')
@endpush