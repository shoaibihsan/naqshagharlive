@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')

<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
  
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN VALIDATION STATES-->
         <div class="widget red">
            <div class="widget-title">
               <h4><i class=" icon-key"></i> Construction </h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body form">
               <!-- BEGIN FORM-->
               @isset($company_profile_contact)
               <form  class="form-horizontal" method="post" action="{{ route('company-profile-contact.edit',$company_profile_contact->id) }}" enctype="multipart/form-data">
               @else
               <form  class="form-horizontal" method="post" action="{{ route('company-profile-contact.store') }}" enctype="multipart/form-data">
               @endisset
                  @csrf
                  <div class="control-group success">
                     <label class="control-label" for="company_profile_id">Company Profile  </label>
                     <div class="controls">
                        <select name="company_profile_id" id="company_profile_id" class="span6 form-control" required="">
                           <option value="">Please Select Company Profile</option>
                           @foreach($companyprofile as $value)
                              <option value="{{ $value->id }}">{{ $value->cp_about_us }}</option>
                           @endforeach
                         </select>
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="c_created_at">Company Creation Date</label>
                     <div class="controls">
                        <input type="date" class="span6" id="c_created_at" name="c_created_at" required="">
                  
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="c_fullname">Company Fullname</label>
                     <div class="controls">
                        <input type="text" class="span6" id="c_fullname" name="c_fullname" required="">
                       
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="c_address">Company Address</label>
                     <div class="controls">
                        <input type="text" class="span6" id="c_address" name="c_address" required="">
                        
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="c_owner_name">Company Owner Name</label>
                     <div class="controls">
                        <input type="text" class="span6" id="c_owner_name" name="c_owner_name" required="">
                        
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="c_mobile_number">Company Mobile Number</label>
                     <div class="controls">
                        <input type="number" class="span6" id="c_mobile_number" name="c_mobile_number" required="">
                       
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="c_email">Company Email</label>
                     <div class="controls">
                        <input type="text" class="span6" id="c_email" name="c_email" required="">
                     </div>
                  </div>
                  
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">
                        @isset($company_profile_contact)
                           Update
                        @else
                        Save
                        @endisset
                     </button>
                     <button type="button" class="btn">Cancel</button>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
         <!-- END VALIDATION STATES-->
      </div>
   </div>

</div>
@endsection

@push('js')
@endpush