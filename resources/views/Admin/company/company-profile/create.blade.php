@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')

<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->

   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN VALIDATION STATES-->
         <div class="widget red">
            <div class="widget-title">
               <h4><i class=" icon-key"></i> Company Profile </h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body form">
               <!-- BEGIN FORM-->
               @isset($company_profile)
                 <form  class="form-horizontal" method="post" action="{{ route('company-profile.update',$company_profile->id) }}" enctype="multipart/form-data">
                   @method('put')
               @else
               <form  class="form-horizontal" method="post" action="{{ route('company-profile.store') }}" enctype="multipart/form-data">
               @endisset 
                  @csrf
                  <div class="control-group success">
                     <label class="control-label" for="company_id">Company </label>
                     <div class="controls">
                        <select class="form-control span6" name="company_id" id="ccompany_id" required="">
                           <option value="">Please Select Company</option>
                           @foreach($company as $value)
                              <option value="{{ $value->id }}">{{ $value->c_name }}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="inputError">Company Profile Slider Image</label>
                     <div class="controls">
                        <input type="file" class="span6" id="inputError" name="cp_slider_image" name="cp_slider_image" required="">
                  
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="cp_about_us">About Us</label>
                     <div class="controls">
                        <textarea name="cp_about_us" id="cp_about_us" class="form-control" required=""></textarea>
                     </div>
                  </div>
                  
                  
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">
                        @isset($company_profile)
                           Update
                        @else 
                        Save
                        @endisset
                     </button>
                     <button type="button" class="btn">Cancel</button>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
         <!-- END VALIDATION STATES-->
      </div>
   </div>

</div>
@endsection

@push('js')
@endpush