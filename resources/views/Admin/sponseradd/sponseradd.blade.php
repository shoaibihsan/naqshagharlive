@extends('Admin.admin-layout') @push('css') @endpush @section('content')

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="widget red">
                <div class="widget-title">
                    <h4><i class=" icon-key"></i>Sponser Add</h4>
                    <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    @if(isset($arch) && !empty($arch))
                    <form class="form-horizontal" method="post" action="{{ route('sponseradd.update',$arch->id) }} " enctype="multipart/form-data">
                        @method('put') @else
                        <form class="form-horizontal" method="post" action="{{ route('sponseradd.store') }}" enctype="multipart/form-data">
                            @endif @csrf
                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Name</label>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                </div>
                                   <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">image_name</label>
                                    <div class="controls">
                                        <input type="file" name="image_name" id="image_name" class="form-control">
                                    </div>
                                </div>
                             
                               
                               

                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success">
                                        @if(isset($arch) && !empty($arch)) Update @else Save @endif
                                    </button>
                                    <button type="button" class="btn">Cancel</button>
                                </div>
                        </form>
                        <!-- END FORM-->
                        </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>

@endsection 
@push('js') 

@endpush


