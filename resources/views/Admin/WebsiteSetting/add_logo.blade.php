@extends('Admin.admin-layout') @push('css') @endpush @section('content')

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <br><br><br>
        <div class="span12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="widget red">
                <div class="widget-title">
                    <h4><i class=" icon-key"></i>Construction</h4>
                    <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    @if(isset($setting) && !empty($setting))
                    <form class="form-horizontal" method="post" action="{{ route('setting.update',$setting->id) }} " enctype="multipart/form-data">
                        @method('put') @else
                        <form class="form-horizontal" method="post" action="{{ route('setting.store') }}" enctype="multipart/form-data">
                            @endif @csrf
                            


                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Title</label>
                                    <div class="controls">
                                        <input type="text" name="title" id="title" class="form-control" @if(isset($setting) && !empty($setting)) value="{{ $setting->title }}" @endif>
                                    </div>
                                </div>
                                   <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Logo</label>
                                    <div class="controls">
                                        <input type="file" name="logo" id="logo" class="form-control">
                                    </div>
                                </div>
                                @if(isset($setting) && !empty($setting))
                                <div class="col-sm-12">
                                    <img src="{{ asset('images/settings'.'/'.$setting->logo) }}" style="width: 100px; height: 100px">
                                </div>
                                @endif
                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Slider Image</label>
                                    <div class="controls">
                                        <input type="file" name="banner_image" id="banner_image" class="form-control">
                                    </div>
                                </div>
                                @if(isset($setting) && !empty($setting))
                                <div class="col-sm-12">
                                    <img src="{{ asset('images/settings'.'/'.$setting->banner_image) }}" style="width: 100px; height: 100px">
                                </div>
                               @endif
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success">
                                        @if(isset($setting) && !empty($setting)) Update @else Save @endif
                                    </button>
                                    <button type="button" class="btn">Cancel</button>
                                </div>
                        </form>
                        <!-- END FORM-->
                        </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>

@endsection 
@push('js') 

@endpush


