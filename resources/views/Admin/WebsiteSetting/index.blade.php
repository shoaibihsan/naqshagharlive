@extends('Admin.admin-layout')
@push('css')
@endpush
@section('content')
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->   
  
   <!-- END PAGE HEADER-->
   <!-- BEGIN EDITABLE TABLE widget-->
   <div class="row-fluid">
            
      <br><br><br>
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE widget-->
         <div class="widget purple">
            <div class="widget-body">
               <div>
                  <div class="clearfix">
                     <div class="btn-group">
                        <a href="{{ route('setting.create') }}" id="editable-sample_new" class="btn green">
                        Add New <i class="icon-plus"></i>
                        </a>
                     </div>
               
                  </div>
                  <div class="space15"></div>
                  <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                     <table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
                        <thead>
                           <tr role="row">
                              <th>#</th>
                              <th>Title</th>
                              <th>Logo</th>
                              <th>Banner Image</th>
                              <th>Edit</th>
                              <th>Delete</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($setting) && !empty($setting))
                           @foreach($setting as $key => $value)
                           <tr>
                              <td> {{ ++$key }} </td>
                              <td> {{ $value->title }} </td>
                              <td>
                                 <img src="{{ asset('images/settings'.'/'.$value->logo) }}" style="width: 100px; height: 100px">
                              </td>
                              <td>
                                 <img src="{{ asset('images/settings'.'/'.$value->banner_image) }}" style="width: 100px; height: 100px">
                              </td>
                              <td>
                                 <a href="{{ route('setting.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                              </td>
                              <td>
                                 <a class="btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-setting-{{ $value->id }}').submit();">Delete
                                    </a>
                                 <form id="delete-setting-{{ $value->id }}" action="{{ route('setting.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>                                
                              
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!-- END EXAMPLE TABLE widget-->
      </div>
   </div>
   <!-- END EDITABLE TABLE widget-->
</div>
@endsection

@push('js')

@endpush