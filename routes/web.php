<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('roue/for/check/php/version/detail',function(){
	ob_start();
    phpinfo();
    return ob_get_clean();
});


// Route::get('/', function () {
//     return view('welcome');
// });
//Admin Routes//

 Route::get('Admin','Admin\AdminController@view');
 Route::resource('Admin/construction','Admin\ConstructionController');
 Route::resource('Admin/Architecture','Admin\ArchitectureController');
 Route::resource('Admin/Interior','Admin\InteriorController');
 Route::resource('Admin/TownPlaning','Admin\TownPlanningController');
 Route::resource('Admin/Landscape','Admin\LandscapeController');
 Route::get('/Admin/company-register','Admin\AdminController@companyprofile');

 Route::resource('admin/company','Admin\CompanyController');
 Route::resource('admin/company-profile-contact','Admin\CompanyProfileContactController');
 Route::resource('admin/company-profile','Admin\CompanyProfileController');
 Route::resource('admin/company-profile-gallery','Admin\CompanyProfileGalleryController');
Route::resource('admin/setting','Admin\WebsiteSettingController');
Route::resource('admin/gallery','Admin\GalleryCategoryController');
Route::resource('admin/gallarysub','Admin\GallerysubController');
Route::resource('admin/gimages','Admin\GalleryimagesController');
Route::resource('admin/sponseradd','Admin\SponserController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// web controller 
Route::post('naqsha-search','Website\HomeController@naqsha_search')->name('naqsha-search');
Route::get('/','Website\HomeController@index')->name('home.index');

Route::get('about-us','Website\HomeController@about')->name('home.about');
Route::get('term-condition','Website\HomeController@term_condition')->name('home.term.condition');
Route::get('faq','Website\HomeController@faq')->name('home.faq');
Route::get('blogs','Website\HomeController@blogs')->name('home.blogs');

Route::get('architecture','Website\HomeController@architecture')->name('home.architecture');
Route::get('construction','Website\HomeController@construction')->name('home.construction');
Route::get('interior','Website\HomeController@interior')->name('home.interior');
Route::get('landscape','Website\HomeController@landscape')->name('home.landscape');
Route::get('town-planner','Website\HomeController@town_planner')->name('home.town.planner');
Route::get('engineers','Website\HomeController@engineers')->name('home.engineers');



Route::get("gallery3d-type/{id}","Website\HomeController@gallery_type")->name('gallery.3d.type');
Route::get("gallery-images/{id}","Website\HomeController@kanal_2_residential")->name('kanal.2.residential');
Route::get("d3_evlevation","Website\HomeController@d3_evlevation")->name('d3.evlevation');
Route::get("one_kanal_house","Website\HomeController@one_kanal_house")->name('one.kanal.house');

Route::get("interior_gallery_type","Website\HomeController@interior_gallery_type")->name('interior.gallery.type');
Route::get("commercial_interior","Website\HomeController@commercial_interior")->name('commercial.interior');
Route::get("residential_interior","Website\HomeController@residential_interior")->name('residential.interior');
Route::get("landscap_gallery","Website\HomeController@landscap_gallery")->name('landscap.gallery');
Route::get("landscapes","Website\HomeController@landscapes")->name('landscapes');

Route::get('company-detail/{id}','Website\HomeController@company_detail')->name('company.detail');

Route::post('sendEmail','Website\HomeController@sendEmail')->name('sendEmail');

Route::post('forBusinessEmail','Website\HomeController@forBusinessEmail')->name('forBusinessEmail');
